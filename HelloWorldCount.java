import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
public class HelloWorldCount{
    
    
    
    public static void myHelloWordsCount(String str){
        String item[] = str.split(" ");
        int size = 0;
        HashMap<String, Integer> map = new HashMap<>();

        for (String t : item) {
            if (map.containsKey(t)) {
                map.put(t, map.get(t) + 1);

            } else {
                map.put(t, 1);
            }
        }
    
        Set<String> keys = map.keySet();
        for (String key : keys) {
            System.out.print(key + " " + map.get(key) );
            size++;
            if (size < keys.size()){
                System.out.print(", ");
            }
           
        }
    }

    public static void main(String args[]) {

        String value ;
        
        if(args.length > 0 )
        value = args[0];
        else{
            value = "hello hello some some some hello some hey ";
            System.out.println("Help hint: " + value);
            System.out.println("Result: " );
        }
    
        myHelloWordsCount(value);

    }
}