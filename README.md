**Words count**

This little java program count all same words. Example:


```java
> java HelloWorldCount "hello hello some some some hello some hey"

some 4, hello 3, hey 1
```
